# Loading and wrangling poverty datasets

## 2017
* [MPI National 2017](national.ipynb)
* [MPI Subnational 2017](subnational.ipynb)
* [Merging the National and Subnational sets](merging_nat_subnat.ipynb)
* [Countries HELP dataset](countries.ipynb)

## 2021
* [MPI Statistics 2021](statistics_2021.ipynb)
* [MPI Disaggregation groups 2021](disaggregation.ipynb)
* [Gender Inequality Index dataset](gender.ipynb)
 