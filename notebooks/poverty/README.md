# Poverty analysis

The Global Multidimensional Poverty Index (MPI) has been published since 2010 and is an internationally-comparable measure of acute poverty covering more than 100 developing countries. It is updated by OPHI twice a year. HELP International is an international humanitarian NGO that is committed to fighting poverty and providing the people of backward countries with basic amenities and relief during the time of disasters and natural calamities, and also provides datasets.

* [Data wrangling](wrangling)
* [Exploratory analyses](analyses)

## Resources

### 2017 data

The [Kaggle Multidimensional Poverty Measures](https://www.kaggle.com/ophi/mpi) was created in 2017 and uploaded by the [Oxford Poverty & Human Development Initiative (OPHI)](https://ophi.org.uk). We used it for initial explorations. We use the mpi_urban feature of the MPI_National dataset as Dependent Variable (DV) in Regression. The Global MPI is an internationally-comparable measure of acute poverty covering more than 100 developing countries. It is updated by OPHI twice a year and constructed using the AF method. More domain knowledge gan be gathered from the [OPHI Online Training Portal](https://ophi.org.uk/courses-and-events/online-training-portal/). The course material are from 2011, 2013 and 2014, but gives a good starting point.

The Kaggle [Unsupervised Learning on Country Data](https://www.kaggle.com/lauraviera/using-ml-to-allocate-funding-for-development-aid/data) from HELP is a dataset for KMeans Clustering, to categorise the countries in it using socio-economic and health factors that determine the overall development of the country. HELP International is an international humanitarian NGO that is committed to fighting poverty and providing the people of backward countries with basic amenities and relief during the time of disasters and natural calamities.

Geopandas naturalearth_lowres dataset has information about each country’s shapes and holds information about the estimated country population and continent. We use it for [making static worldmaps](https://geopandas.org/en/stable/docs/user_guide/mapping.html).

### 2021 data

The Global Multidimensional Poverty Index MPI has been published since 2010 in the [United Nations Development Programme’s Human Development Data Center](https://hdr.undp.org/en/data).

We are using the [Multidimensional Poverty Index 2021 statistical data tables 1 and 2 (.xlsx)](https://hdr.undp.org/sites/default/files/2021_mpi_statistical_data_table_1_and_2_en.xlsx), and to explore how poverty correlates with gender and ethnic groups also the [Gender Inequality Index (GII) (.xlsx)](https://hdr.undp.org/sites/default/files/2020_statistical_annex_table_5.xlsx) and [Multidimensional Poverty Index: Disaggregation by ethnic/racial/caste group (.xlsx)](https://hdr.undp.org/sites/default/files/multidimensional_poverty_index_disaggregation_by_ethnic_racial_caste_groups.xlsx)



