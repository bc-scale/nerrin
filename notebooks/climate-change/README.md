# Climate change predictions

For climate change analysis the first big challenge is to find, collect and combine useful data.
Other challenges in this project are to create and subtract naturally caused time series from projections time series to calculate and visualize human-made trends, and the complex interconnections of and reinforcing feedback loops in causes and effects.

* Data wrangling
* [Climate change analysis](current-state)
* [Possible causes of climate change](causes)
* [Possible effects of climate change](effects)

## Resources

### Collecting datasets

* [The Berkeley Earth Surface Temperature Study](https://berkeleyearth.org/about/) combines 1.6 billion temperature reports from 16 pre-existing archives. It is nicely packaged and allows for slicing into interesting subsets (for example by country).
* [Ice core records](https://catalog-prod-datagov.app.cloud.gov/dataset/?tags=ice-core-records) from US Antarctic Program Data Center, Berkeley Space Sciences Laboratory, NSIDC, and US National Snow and Ice Data Center
* [Sea Level Change Data Pathfinder](https://earthdata.nasa.gov/learn/pathfinders/sea-level-change) from NASA in collaboration with other federal entities and international space organizations, including NOAA, USGS, the Japan Aerospace Exploration Agency (JAXA) and Ministry of Economy, Trade, and Industry (METI), and the European Space Agency (ESA).
* [Atmospheric greenhouse gas measurements](https://www.csiro.au/greenhouse-gases/) from CSIRO in Tasmania.

### Articles

* [The influence of decision-making in tree ring-based climate reconstructions](https://www.nature.com/articles/s41467-021-23627-6)
* [Air bubbles in Antarctic ice point to cause of oxygen decline](https://www.sciencedaily.com/releases/2021/12/211220190643.htm)