# Possible effects of climate change

* If the current trends continue, what are the projections that can be made regarding temperatures, shifting precipitaton patterns, ocean acidification, sea level patterns and intensity and frequency of extreme weather events?
* Which countries are likely to be hit the hardest?
* Which countries are likely to be hit the least?
